﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MACEG
{
    
    public partial class Default : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                loadFData();
                loadSlider();
                loadLatestNews();
                loadReviews();
            }
        }
        protected void loadFData() {
            DataSet dsPart = codeClass.SQLREAD("select * from Partners");
            lvPartners.DataSource = dsPart;
            lvPartners.DataBind();
        }
        protected void loadLatestNews() {
            DataSet ds = codeClass.SQLREAD("select top(4) * from News order by date desc");
            lvNewssList.DataSource = ds;
            lvNewssList.DataBind();
    }
        protected void loadSlider() {
            DataSet ds = codeClass.SQLREAD("SELECT * from Slider");
            lvSlider.DataSource = ds;
            lvSlider.DataBind();
        }
        protected void loadReviews() {
            DataSet ds = codeClass.SQLREAD("select top(6)* from reviews where revType = N'Review' order by ID desc");
            lvReviews.DataSource = ds;
            lvReviews.DataBind();
        }

        protected void lvReviews_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListView lvRating = e.Item.FindControl("lvRating") as ListView;
            HiddenField hf = e.Item.FindControl("hfStars") as HiddenField;
            int stars = int.Parse(hf.Value);
            DataSet dsStars = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("stars", typeof(int));
            for (int i = 0; i < stars; i++)
            {
                dt.Rows.Add((i + 1));
            }
            dsStars.Tables.Add(dt);
            lvRating.DataSource = dsStars;
            lvRating.DataBind();
        }
    }
}