﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="MACEG.Product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Product Name 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        #main-content {
            padding-top: 5%;
        }

        body .product_meta span {
            display: -webkit-box;
        }

        /*body .product {
            height:250px;
            width:33%;
            background-size:cover;
        }
        body .product img {
            height:250px;
            width:33%;
            background-size:cover;
            padding:initial;
            margin:initial;
        }*/
        body .search-field {
            width: 80%;
            border-radius: 5px !important;
        }

        body .header-search.icon.wpv-overlay-search-trigger {
            background: none;
            border: none;
            font-size: 20pt;
            vertical-align: middle;
        }

        body .product-thumbnail {
            margin-bottom: unset !important;
        }

            body .product-thumbnail img {
                margin: unset !important;
            }

        .woocommerce ul.products li.product .add_to_cart_button, .woocommerce ul.products li.product .vamtam-button.product_type_simple {
            margin-top: 0 !important;
        }

        body .title, body .page-header {
            padding: 0 !important;
            margin: 0 !important;
        }

        body .title {
            margin-bottom: 20px;
        }
    </style>
    <style>
        body .page-header-content {
            margin-top: -20px;
            margin-bottom: 20px;
        }

        body .page-content.no-image {
            padding-top: 20px;
        }

        body .summary.entry-summary {
            padding-top: 20px;
        }

        body #product-gallery-pager-8995 {
            display: inline-flex;
        }

        body .bxslider-wrapper {
            max-height: 300px;
        }

        body ul.tabs.wc-tabs {
            list-style-type: none;
            margin: 10px 0px;
            padding: 0px;
        }

        body ul, ol {
            list-style-type: none;
        }

            body ul.tabs.wc-tabs li {
                width: fit-content;
                display: initial;
                padding-right: 10px;
                padding-bottom: 10px;
                font-size: 100%;
                color: #2b2f34;
                margin: 20px 0px;
            }

                body ul.tabs.wc-tabs li.active {
                    border-bottom: #3B6097 3px solid;
                }

        body .woocommerce-Tabs-panel {
            border-top: 1px solid #cecece;
        }

        body ul.tabs.wc-tabs li a {
            font-size: 15pt;
            color: #2b2f34;
            margin: 20px 0px;
            cursor: pointer;
        }

        body .form-submit {
            margin: 0px !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("[role='tab']").click(function () {
                var thisLi = $(this);
                var div2Call = $(thisLi).attr("aria-controls");
                //alert(div2Call);
                $(thisLi).siblings().removeClass("active");
                $(thisLi).addClass("active");
                $(".woocommerce-Tabs-panel").css("display", "none");
                $("#" + div2Call).css("display", "block");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">

        <div id="main" role="main" class="wpv-main layout-left-only">
            <div class="limit-wrapper">
                <div class="row page-wrapper">
                    <aside class="left">
                        <section id="woocommerce_product_categories-4" class="widget woocommerce widget_product_categories">
                            <h4 class="widget-title">CATEGORIES</h4>
                            <ul class="product-categories">
                                <li class="cat-item cat-item-441"><a href="https://construction.vamtam.com/product-category/garden-garage/">Garden &amp; Garage</a></li>
                                <li class="cat-item cat-item-419"><a href="https://construction.vamtam.com/product-category/hand-tools/">Hand tools</a></li>
                                <li class="cat-item cat-item-428 current-cat"><a href="https://construction.vamtam.com/product-category/protective/">Protective</a></li>
                                <li class="cat-item cat-item-438"><a href="https://construction.vamtam.com/product-category/saws/">Saws</a></li>
                                <li class="cat-item cat-item-363"><a href="https://construction.vamtam.com/product-category/screwdrivers/">Screwdrivers</a></li>
                            </ul>
                        </section>
                        <section id="woocommerce_product_search-4" class="widget woocommerce widget_product_search">
                            <h4 class="widget-title">SEARCH</h4>
                            <form role="search" method="get" class="woocommerce-product-search" action="https://construction.vamtam.com/">
                                <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
                                <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products&hellip;" value="" name="s" />
                                <button class="header-search icon wpv-overlay-search-trigger">&#57645;</button>
                                <input type="hidden" name="post_type" value="product" />
                            </form>
                        </section>
                        <section id="woocommerce_product_tag_cloud-3" class="widget woocommerce widget_product_tag_cloud">
                            <h4 class="widget-title">TAGS</h4>
                            <div class="tagcloud">
                                <a href="https://construction.vamtam.com/product-tag/brake-pads/" class="tag-cloud-link tag-link-400 tag-link-position-1" style="font-size: 22pt;" aria-label="Brake Pads (6 products)">Brake Pads</a>
                                <a href="https://construction.vamtam.com/product-tag/fog-lights/" class="tag-cloud-link tag-link-442 tag-link-position-2" style="font-size: 8pt;" aria-label="Fog Lights (1 product)">Fog Lights</a>
                                <a href="https://construction.vamtam.com/product-tag/green/" class="tag-cloud-link tag-link-469 tag-link-position-3" style="font-size: 8pt;" aria-label="green (1 product)">green</a>
                                <a href="https://construction.vamtam.com/product-tag/lights/" class="tag-cloud-link tag-link-460 tag-link-position-4" style="font-size: 8pt;" aria-label="Lights (1 product)">Lights</a>
                                <a href="https://construction.vamtam.com/product-tag/oil/" class="tag-cloud-link tag-link-402 tag-link-position-5" style="font-size: 22pt;" aria-label="Oil (6 products)">Oil</a>
                                <a href="https://construction.vamtam.com/product-tag/oil-filters/" class="tag-cloud-link tag-link-443 tag-link-position-6" style="font-size: 8pt;" aria-label="Oil Filters (1 product)">Oil Filters</a>
                                <a href="https://construction.vamtam.com/product-tag/pipes/" class="tag-cloud-link tag-link-474 tag-link-position-7" style="font-size: 18.181818181818pt;" aria-label="Pipes (4 products)">Pipes</a>
                                <a href="https://construction.vamtam.com/product-tag/saws/" class="tag-cloud-link tag-link-478 tag-link-position-8" style="font-size: 12.581818181818pt;" aria-label="Saws (2 products)">Saws</a>
                                <a href="https://construction.vamtam.com/product-tag/shocks/" class="tag-cloud-link tag-link-401 tag-link-position-9" style="font-size: 22pt;" aria-label="Shocks &amp; Struts (6 products)">Shocks &amp; Struts</a>
                                <a href="https://construction.vamtam.com/product-tag/spark-plugs/" class="tag-cloud-link tag-link-472 tag-link-position-10" style="font-size: 18.181818181818pt;" aria-label="Spark Plugs (4 products)">Spark Plugs</a>
                                <a href="https://construction.vamtam.com/product-tag/tags/" class="tag-cloud-link tag-link-467 tag-link-position-11" style="font-size: 8pt;" aria-label="tags (1 product)">tags</a>
                                <a href="https://construction.vamtam.com/product-tag/tools/" class="tag-cloud-link tag-link-468 tag-link-position-12" style="font-size: 8pt;" aria-label="tools (1 product)">tools</a>
                            </div>
                        </section>
                    </aside>

                    <article class="left-only">
                        <div id="sub-header" class="layout-left-only has-background">
                            <div class="meta-header" style="">
                                <div class="limit-wrapper">
                                    <div class="meta-header-inside">
                                        <header class="page-header ">
                                            <div class="page-header-content">
                                                <h1 style="">
                                                    <span class="title">
                                                        <span itemprop="headline">
                                                            <asp:Literal Text="Electric fret saw" ID="productLbl" runat="server" /></span>
                                                    </span>
                                                </h1>
                                            </div>
                                        </header>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="page-content no-image">
                            <div id="product-8995" class="post-8995 product type-product status-publish has-post-thumbnail product_cat-protective product_tag-pipes product_tag-saws product_tag-spark-plugs pa_brand-topmaster first outofstock sold-individually shipping-taxable purchasable product-type-simple">
                                <asp:ListView ID="lvProdData" ClientIDMode="Static" OnItemDataBound="lvProdData_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1;">
                                                    <div class="bxslider-wrapper">
                                                        <div class="bx-wrapper" style="max-width: 100%;">
                                                            <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 382px;">
                                                                <ul class="bxslider-container" id="product-gallery-8995" style="width: 3215%; position: absolute; transition-duration: 0s; transform: translate3d(-382px, 0px, 0px);">
                                                                    <li style="float: left; list-style: none; position: relative; width: 382px;" class="bx-clone">
                                                                        <div data-thumb="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg" class="woocommerce-product-gallery__image">
                                                                            <a href="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg">
                                                                                <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg" class="wp-post-image" alt="" title="product-4.3" data-caption="" data-src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg" data-large_image="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg" data-large_image_width="300" data-large_image_height="300" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px"></a>
                                                                        </div>
                                                                    </li>
                                                                    <li style="float: left; list-style: none; position: relative; width: 382px;">
                                                                        <div data-thumb="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg" class="woocommerce-product-gallery__image">
                                                                            <a href="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg">
                                                                                <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg" class="wp-post-image" alt="" title="product-4.1" data-caption="" data-src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg" data-large_image="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg" data-large_image_width="300" data-large_image_height="300" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px"></a>
                                                                        </div>
                                                                    </li>
                                                                    <li style="float: left; list-style: none; position: relative; width: 382px;">
                                                                        <div data-thumb="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg" class="woocommerce-product-gallery__image">
                                                                            <a href="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-300x300.jpg">
                                                                                <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-300x300.jpg" class="wp-post-image" alt="" title="product-4.2" data-caption="" data-src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-300x300.jpg" data-large_image="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-300x300.jpg" data-large_image_width="300" data-large_image_height="300" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px"></a>
                                                                        </div>
                                                                    </li>
                                                                    <li style="float: left; list-style: none; position: relative; width: 382px;">
                                                                        <div data-thumb="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg" class="woocommerce-product-gallery__image">
                                                                            <a href="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg">
                                                                                <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg" class="wp-post-image" alt="" title="product-4.3" data-caption="" data-src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg" data-large_image="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-300x300.jpg" data-large_image_width="300" data-large_image_height="300" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px"></a>
                                                                        </div>
                                                                    </li>
                                                                    <li style="float: left; list-style: none; position: relative; width: 382px;" class="bx-clone">
                                                                        <div data-thumb="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg" class="woocommerce-product-gallery__image">
                                                                            <a href="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg">
                                                                                <img width="300" height="300" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg" class="wp-post-image" alt="" title="product-4.1" data-caption="" data-src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg" data-large_image="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-300x300.jpg" data-large_image_width="300" data-large_image_height="300" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43@2x.jpg 86w" sizes="(max-width: 300px) 100vw, 300px"></a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="bx-controls"></div>
                                                        </div>
                                                        <script>
                                                            jQuery(function ($) {
                                                                var el = $('#product-gallery-8995');
                                                                el.data('bxslider', el.bxSlider({
                                                                    pagerCustom: '#product-gallery-pager-8995',
                                                                    controls: false,
                                                                    adaptiveHeight: true
                                                                }));
                                                            });
                                                        </script>
                                                    </div>
                                                    <div class="thumbnails" id="product-gallery-pager-8995">
                                                        <a data-slide-index="0" href="" class="first active" title="product-4.1">
                                                            <img width="84" height="84" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150@2x.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43@2x.jpg 86w" sizes="(max-width: 150px) 100vw, 150px"></a><a data-slide-index="1" href="" class="" title="product-4.2"><img width="84" height="84" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-150x150@2x.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.2-43x43@2x.jpg 86w" sizes="(max-width: 150px) 100vw, 150px"></a><a data-slide-index="2" href="" class="" title="product-4.3"><img width="84" height="84" src="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-150x150@2x.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="" srcset="https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-150x150.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-250x250.jpg 250w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-400x400.jpg 400w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-290x290.jpg 290w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-150x150@2x.jpg 300w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-60x60.jpg 60w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-43x43.jpg 43w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-360x360.jpg 360w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-262x262.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3.jpg 450w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-60x60@2x.jpg 120w, https://construction.vamtam.com/wp-content/uploads/2015/01/product-4.3-43x43@2x.jpg 86w" sizes="(max-width: 150px) 100vw, 150px"></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="summary entry-summary">
                                                    <p class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span><%# Eval("price") %></span></p>
                                                    <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                                        <div class="star-rating" title="Rated 4.00 out of 5"><span style="width: 80%"><strong itemprop="ratingValue" class="rating">4.00</strong> <span class="visuallyhidden">out of 5</span></span></div>
                                                    </div>
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Mauris molestie sit amet metus mattis varius. Donec sit amet ligula eget nisi sodales egestas.</p>
                                                        <h4>Description Brief</h4>
                                                        <p><%# Eval("descriptionTitle") %></p>
                                                    </div>
                                                    <p class="stock out-of-stock"><small>Out of stock</small></p>
                                                    <div class="product_meta">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <th>Brand</th>
                                                                    <td><span class="sku"><%# Eval("BrandName") %></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Category</th>
                                                                    <td><a href="https://construction.vamtam.com/product-category/protective/" rel="tag"><%# Eval("catID") %></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Tags</th>
                                                                    <td><a href="#" rel="tag"><%# Eval("tagsy") %></a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <%--<span class="sku_wrapper">Brand: <span class="sku"><%# Eval("BrandName") %></span></span>--%>
                                                        <%--<span class="posted_in">Category: <a href="https://construction.vamtam.com/product-category/protective/" rel="tag"><%# Eval("catID") %></a></span>--%>
                                                        <%--<span class="tagged_as">Tags: <a href="#" rel="tag"><%# Eval("tagsy") %></a></span>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="woocommerce-tabs wc-tabs-wrapper">
                                            <ul class="tabs wc-tabs" role="tablist">
                                                <li class="description_tab active" id="tab-title-description" role="tab" aria-controls="tab-description">
                                                    <a>Description</a>
                                                </li>
                                                <li class="reviews_tab" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
                                                    <a>Reviews (1)</a>
                                                </li>
                                            </ul>
                                            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description" style="display: block;">
                                                <h2>Description</h2>
                                                <p><%# Eval("description") %></p>
                                                <br />
                                                <h2>Specifications</h2>
                                                <table class="table">
                                                    <tbody>
                                                        <asp:ListView ID="lvSpecs" ClientIDMode="Static" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <th><%# Eval("specName") %></th>
                                                                    <td><%# Eval("specValue") %></td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content wc-tab" id="tab-reviews" role="tabpanel" aria-labelledby="tab-title-reviews" style="display: none;">
                                                <div id="reviews" class="woocommerce-Reviews">
                                                    <div id="comments">
                                                        <h2 class="woocommerce-Reviews-title">1 review for <span>Electric fret saw</span></h2>
                                                        <ol class="commentlist">
                                                            <li itemprop="review" itemscope="" itemtype="http://schema.org/Review" class="comment byuser comment-author-admin bypostauthor even thread-even depth-1" id="li-comment-79">
                                                                <div id="comment-79" class="comment_container clearfix">
                                                                    <div class="col-md-3 col-sm-6">
                                                                        <div class="comment-author">
                                                                            <img alt="" src="https://secure.gravatar.com/avatar/89cd5c4f609a916dbe9e39810830f201?s=60&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/89cd5c4f609a916dbe9e39810830f201?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60">
                                                                            <h5 class="comment-author-link" itemprop="author">Gary Jones</h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-6">
                                                                        <div class="comment-content">
                                                                            <div class="comment-meta">
                                                                                <div class="star-rating"><span style="width: 80%">Rated <strong class="rating">4</strong> out of 5</span></div>
                                                                                <p class="meta">
                                                                                    <strong class="woocommerce-review__author">Gary Jones</strong> <span class="woocommerce-review__dash">–</span> <time class="woocommerce-review__published-date" datetime="2015-03-08T19:43:37+00:00">March 8, 2015</time>
                                                                                </p>
                                                                            </div>
                                                                            <div class="description">
                                                                                <p>Great product!</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ol>
                                                    </div>
                                                    <hr />
                                                    <div id="review_form_wrapper">
                                                        <div id="review_form">
                                                            <div id="respond" class="comment-respond">
                                                                <span id="reply-title" class="comment-reply-title">Add a review <small><a rel="nofollow" id="cancel-comment-reply-link" href="/product/electric-fret-saw/#respond" style="display: none;">Cancel reply</a></small></span>
                                                                <form action="https://construction.vamtam.com/wp-comments-post.php" method="post" id="commentform" class="comment-form">
                                                                    <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p>
                                                                    <p class="comment-form-rating">
                                                                        <label for="rating">Your Rating</label><p class="stars"><span><a class="star-1" href="#">1</a><a class="star-2" href="#">2</a><a class="star-3" href="#">3</a><a class="star-4" href="#">4</a><a class="star-5" href="#">5</a></span></p>
                                                                        <select name="rating" id="rating" style="display: none;">
                                                                            <option value="">Rate…</option>
                                                                            <option value="5">Perfect</option>
                                                                            <option value="4">Good</option>
                                                                            <option value="3">Average</option>
                                                                            <option value="2">Not that bad</option>
                                                                            <option value="1">Very Poor</option>
                                                                        </select>
                                                                    </p>
                                                                    <p class="comment-form-comment">
                                                                        <label for="comment">Your Review</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" required=""></textarea>
                                                                    </p>
                                                                    <p class="comment-form-author">
                                                                        <label for="author">Name <span class="required">*</span></label>
                                                                        <input id="author" name="author" type="text" value="" size="30" aria-required="true" required="">
                                                                    </p>
                                                                    <p class="comment-form-email">
                                                                        <label for="email">Email <span class="required">*</span></label>
                                                                        <input id="email" name="email" type="email" value="" size="30" aria-required="true" required="">
                                                                    </p>
                                                                    <p class="form-submit">
                                                                        <input name="submit" type="submit" id="submit" class="submit" value="Submit">
                                                                        <input type="hidden" name="comment_post_ID" value="8995" id="comment_post_ID">
                                                                        <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                                                    </p>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                                <section class="up-sells upsells products">
                                    <h2>You may also like…</h2>
                                    <ul class="products">
                                        <div class="col-md-4 col-sm-12">
                                            <li class="post-8974 product type-product status-publish has-post-thumbnail product_cat-saws product_cat-screwdrivers product_tag-pipes product_tag-spark-plugs pa_brand-hilti pa_brand-sikkens first instock shipping-taxable purchasable product-type-simple">
                                                <a href="https://construction.vamtam.com/product/red-safety-helmet/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                    <div class="product-thumbnail">
                                                        <img width="290" height="290" src="//construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px">
                                                    </div>
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>7.20</span></span>
                                                    <h5>Hilti, Sikkens</h5>
                                                    <h3 class="woocommerce-loop-product__title">Red safety helmet</h3>
                                                </a><a href="/product/electric-fret-saw/?add-to-cart=8974" rel="nofollow" data-product_id="8974" data-product_sku="458" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                        </div>
                                    </ul>
                                </section>
                                <div class="row ">
                                    <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-c60f940e2581185ddcab2637caa8fe0f">
                                        <h2 class="text-divider-double">RELATED PRODUCTS</h2>
                                        <div class="sep"></div>
                                        <div class="woocommerce columns-3 ">
                                            <ul class="products">
                                                <li class="post-10025 product type-product status-publish has-post-thumbnail product_cat-garden-garage product_cat-hand-tools product_cat-saws product_tag-green product_tag-tags product_tag-tools first instock sale shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/product-no-sidebar/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <span class="onsale">Sale!</span>
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-8.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>56.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>33.00</span></ins></span>
                                                        <h5>Helmet</h5>
                                                        <h3 class="woocommerce-loop-product__title">Product &#8211; no sidebar</h3>
                                                    </a>
                                                    <div style="display: none;" class="aggregateRating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                        <%--<div class="star-rating" title="Rated 4.00 out of 5"><span style="width: 80%"><strong itemprop="ratingValue" class="rating">4.00</strong> <span class="visuallyhidden">out of 5</span></span></div>--%>
                                                    </div>
                                                    <a href="index410c.html?add-to-cart=10025" rel="nofollow" data-product_id="10025" data-product_sku="10923" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                                <li class="post-8995 product type-product status-publish has-post-thumbnail product_cat-protective product_tag-pipes product_tag-saws product_tag-spark-plugs pa_brand-topmaster  outofstock sold-individually shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/electric-fret-saw/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-4.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-4.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>48.00</span></span>
                                                        <h5>TopMaster</h5>
                                                        <h3 class="woocommerce-loop-product__title">Electric fret saw</h3>
                                                    </a>
                                                    <div style="display: none;" class="aggregateRating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                        <%--<div class="star-rating" title="Rated 4.00 out of 5"><span style="width: 80%"><strong itemprop="ratingValue" class="rating">4.00</strong> <span class="visuallyhidden">out of 5</span></span></div>--%>
                                                    </div>
                                                    <a href="../product/electric-fret-saw/index.html" rel="nofollow" data-product_id="8995" data-product_sku="456" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple ajax_add_to_cart"><span class="btext" data-text="Read more">Read more</span></a></li>
                                                <li class="post-8994 product type-product status-publish has-post-thumbnail product_cat-protective product_tag-pipes product_tag-spark-plugs pa_brand-bosch last instock sold-individually shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/wireless-screwdriver/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-5.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-5.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>4.90</span></span>
                                                        <h5>Bosch</h5>
                                                        <h3 class="woocommerce-loop-product__title">Wireless Screwdriver</h3>
                                                    </a><a href="indexf5c6.html?add-to-cart=8994" rel="nofollow" data-product_id="8994" data-product_sku="458" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                                <li class="post-8993 product type-product status-publish has-post-thumbnail product_cat-protective product_tag-fog-lights product_tag-oil-filters pa_brand-sparky first instock sale shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/work-gloves/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <span class="onsale">Sale!</span>
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-6.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-6.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>16.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>9.99</span></ins></span>
                                                        <h5>Sparky</h5>
                                                        <h3 class="woocommerce-loop-product__title">Work gloves</h3>
                                                    </a><a href="indexd33d.html?add-to-cart=8993" rel="nofollow" data-product_id="8993" data-product_sku="456" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                                <li class="post-8992 product type-product status-publish has-post-thumbnail product_cat-hand-tools product_tag-pipes product_tag-saws product_tag-spark-plugs pa_brand-bosch pa_brand-sikkens pa_brand-sparky  instock shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/protective-helmet/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-7.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-7.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>24.40</span></span>
                                                        <h5>Bosch, Sikkens, Sparky</h5>
                                                        <h3 class="woocommerce-loop-product__title">Protective helmet</h3>
                                                    </a><a href="indexede7.html?add-to-cart=8992" rel="nofollow" data-product_id="8992" data-product_sku="456" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                                <li class="post-8974 product type-product status-publish has-post-thumbnail product_cat-saws product_cat-screwdrivers product_tag-pipes product_tag-spark-plugs pa_brand-hilti pa_brand-sikkens last instock shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/red-safety-helmet/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-8.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-8.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>7.20</span></span>
                                                        <h5>Hilti, Sikkens</h5>
                                                        <h3 class="woocommerce-loop-product__title">Red safety helmet</h3>
                                                    </a><a href="indexc0a2.html?add-to-cart=8974" rel="nofollow" data-product_id="8974" data-product_sku="458" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                                <li class="post-6793 product type-product status-publish has-post-thumbnail product_cat-hand-tools product_tag-brake-pads product_tag-oil product_tag-shocks pa_brand-bosch pa_brand-sparky first instock shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/protective-eyeglasses/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-9.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-9.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>20.00</span></span>
                                                        <h5>Bosch, Sparky</h5>
                                                        <h3 class="woocommerce-loop-product__title">Protective Eyeglasses</h3>
                                                    </a><a href="index97ed.html?add-to-cart=6793" rel="nofollow" data-product_id="6793" data-product_sku="" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                                <li class="post-9940 product type-product status-publish has-post-thumbnail product_cat-garden-garage product_cat-hand-tools product_cat-saws product_tag-brake-pads product_tag-oil product_tag-shocks pa_brand-bosch pa_brand-sparky  instock shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/gasoline-chain-saw/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-10.1-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-10.1-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>20.00</span></span>
                                                        <h5>Bosch, Sparky</h5>
                                                        <h3 class="woocommerce-loop-product__title">Gasoline chain saw</h3>
                                                    </a><a href="index6cef.html?add-to-cart=9940" rel="nofollow" data-product_id="9940" data-product_sku="" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                                <li class="post-9941 product type-product status-publish has-post-thumbnail product_cat-garden-garage product_cat-hand-tools product_cat-saws product_tag-brake-pads product_tag-oil product_tag-shocks pa_brand-sparky last instock shipping-taxable purchasable product-type-simple col-md-4 col-sm-6 col-xs-12">
                                                    <a href="../product/concrete-mixer/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                        <div class="product-thumbnail">
                                                            <img width="290" height="290" src="../wp-content/uploads/2015/01/product-11-290x290.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" srcset="//construction.vamtam.com/wp-content/uploads/2015/01/product-11-290x290.jpg 290w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-250x250.jpg 250w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-400x400.jpg 400w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-150x150.jpg 150w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-150x150@2x.jpg 300w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-60x60.jpg 60w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-43x43.jpg 43w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-360x360.jpg 360w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-262x262.jpg 262w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11.jpg 450w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-60x60@2x.jpg 120w, //construction.vamtam.com/wp-content/uploads/2015/01/product-11-43x43@2x.jpg 86w" sizes="(max-width: 290px) 100vw, 290px" />
                                                        </div>
                                                        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>20.00</span></span>
                                                        <h5>Sparky</h5>
                                                        <h3 class="woocommerce-loop-product__title">Concrete mixer</h3>
                                                    </a><a href="index26f5.html?add-to-cart=9941" rel="nofollow" data-product_id="9941" data-product_sku="" data-quantity="1" class="vamtam-button button-border accent1 hover-accent1  product_type_simple add_to_cart_button ajax_add_to_cart"><span class="btext" data-text="Add to cart">Add to cart</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
