﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MACEG
{
    public partial class passReset : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string xCode = Request.QueryString["XCode"].ToString();
                DataSet ds = codeClass.SQLREAD("select * from passResett where hashedy = N'" + xCode + "'");
                if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
                {
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    txtMail.Value = ds.Tables[0].Rows[0]["email"].ToString();
                }
            }
        }

        protected void btnReset_ServerClick(object sender, EventArgs e)
        {
            string str = codeClass.SQLINSERT("update Users set passwordy = N'" + txtPass.Value + "' where email = N'"+txtMail.Value+"'");
            Response.Redirect("Account.aspx");
        }
    }
}