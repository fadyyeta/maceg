﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="LOB.aspx.cs" Inherits="MACEG.LOB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Lines of Business
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">
        <div id="main" role="main" class="wpv-main layout-full">
            <div class="extended-column-inner">
                <style>
                    #wpv-column-b087a43a312a0e3a878e2c5592583766 p, #wpv-column-b087a43a312a0e3a878e2c5592583766 em, #wpv-column-b087a43a312a0e3a878e2c5592583766 h1, #wpv-column-b087a43a312a0e3a878e2c5592583766 h2, #wpv-column-b087a43a312a0e3a878e2c5592583766 h3, #wpv-column-b087a43a312a0e3a878e2c5592583766 h4, #wpv-column-b087a43a312a0e3a878e2c5592583766 h5, #wpv-column-b087a43a312a0e3a878e2c5592583766 h6, #wpv-column-b087a43a312a0e3a878e2c5592583766 .column-title, #wpv-column-b087a43a312a0e3a878e2c5592583766 .sep-text h2.regular-title-wrapper, #wpv-column-b087a43a312a0e3a878e2c5592583766 .text-divider-double, #wpv-column-b087a43a312a0e3a878e2c5592583766 .sep-text .sep-text-line, #wpv-column-b087a43a312a0e3a878e2c5592583766 .sep, #wpv-column-b087a43a312a0e3a878e2c5592583766 .sep-2, #wpv-column-b087a43a312a0e3a878e2c5592583766 .sep-3, #wpv-column-b087a43a312a0e3a878e2c5592583766 td, #wpv-column-b087a43a312a0e3a878e2c5592583766 th, #wpv-column-b087a43a312a0e3a878e2c5592583766 caption {
                        color: #808080;
                    }

                    #wpv-column-b087a43a312a0e3a878e2c5592583766:before {
                        background-color: transparent;
                    }
                </style>
                <div class="row ">
                    <div class="wpv-grid grid-1-3  first has-background unextended has-extended-padding" style="background: url('https://construction.vamtam.com/wp-content/uploads/2015/01/shop_bg_banner1.jpg') right center / cover no-repeat scroll rgb(145, 145, 145); background-size: cover; background-attachment: scroll; background-color: #919191; padding-top: 8%; padding-bottom: 20px; min-height: 400px; width: 50%;"
                        id="wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58">
                        <style>
                            #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 p, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 em, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 h1, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 h2, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 h3, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 h4, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 h5, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 h6, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 .column-title, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 .sep-text h2.regular-title-wrapper, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 .text-divider-double, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 .sep-text .sep-text-line, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 .sep, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 .sep-2, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 .sep-3, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 td, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 th, #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58 caption {
                                color: #fff;
                            }

                            #wpv-column-56a2b9ae1b338900f9fd1f49b8ea7f58:before {
                                background-color: transparent;
                            }
                        </style>
                        <div class="row " style="vertical-align: middle">
                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-27d19ed190f8ca002bfed535a8b9d684">
                                <p style="text-align: center;"><span class="vamtam-font-style-2 with-color">B2B</span></p>
                                <h2 class="accent-5" style="text-align: center;">BUSINESS TO BUSINESS</h2>
                                <div class="push " style="height: 5px"></div>
                                <p class="textcenter"><a href="Brands.aspx?LOB=B2B" target="_self" style='font-size: 12px;' class="vamtam-button accent5  button-border hover-accent1 "><span class="btext" data-text="SEE AVAILABLE BRANDS">SEE AVAILABLE BRANDS</span></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="wpv-grid grid-1-3  has-background unextended has-extended-padding" style="background: url('https://construction.vamtam.com/wp-content/uploads/2015/01/shop_bg_banner2.jpg') right center / cover no-repeat scroll rgb(145, 145, 145); background-size: cover; background-attachment: scroll; background-color: #919191; padding-top: 8%; padding-bottom: 20px; min-height: 400px; width: 50%;"
                        id="wpv-column-11a48caa7cd592bfbdc750a253cb64f1">
                        <style>
                            #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 p, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 em, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 h1, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 h2, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 h3, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 h4, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 h5, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 h6, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 .column-title, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 .sep-text h2.regular-title-wrapper, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 .text-divider-double, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 .sep-text .sep-text-line, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 .sep, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 .sep-2, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 .sep-3, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 td, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 th, #wpv-column-11a48caa7cd592bfbdc750a253cb64f1 caption {
                                color: #fff;
                            }

                            #wpv-column-11a48caa7cd592bfbdc750a253cb64f1:before {
                                background-color: transparent;
                            }
                        </style>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-832512c45e5eae2035faded7db4fcfaa">
                                <p style="text-align: center;"><span class="vamtam-font-style-2 with-color">B2C</span></p>
                                <h2 class="accent-5" style="text-align: center;">BUSINESS TO CONSUMER</h2>
                                <div class="push " style="height: 5px"></div>
                                <p class="textcenter"><a href="Brands.aspx?LOB=B2C" target="_self" style='font-size: 12px;' class="vamtam-button accent5  button-border hover-accent1 "><span class="btext" data-text="SEE AVAILABLE BRANDS">SEE AVAILABLE BRANDS</span></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="wpv-grid grid-1-3  has-background unextended has-extended-padding" style="background: url('https://construction.vamtam.com/wp-content/uploads/2015/01/shop_bg_banner3.jpg') right center / cover no-repeat scroll rgb(145, 145, 145); background-size: cover; background-attachment: scroll; background-color: #919191; padding-top: 8%; padding-bottom: 20px; min-height: 300px; display: none;"
                        id="wpv-column-e53ed0f4679ffde4275730a53d178bf1">
                        <style>
                            #wpv-column-e53ed0f4679ffde4275730a53d178bf1 p, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 em, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 h1, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 h2, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 h3, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 h4, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 h5, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 h6, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 .column-title, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 .sep-text h2.regular-title-wrapper, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 .text-divider-double, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 .sep-text .sep-text-line, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 .sep, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 .sep-2, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 .sep-3, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 td, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 th, #wpv-column-e53ed0f4679ffde4275730a53d178bf1 caption {
                                color: #fff;
                            }

                            #wpv-column-e53ed0f4679ffde4275730a53d178bf1:before {
                                background-color: transparent;
                            }
                        </style>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-827b00d28f68faea1df7e61e9a9eb54f">
                                <p style="text-align: center;"><span class="vamtam-font-style-2 with-color">-30%</span></p>
                                <h2 class="accent-5" style="text-align: center;">LOYALTY PROGRAM</h2>
                                <div class="push " style="height: 5px"></div>
                                <p class="textcenter"><a href="#" target="_self" style='font-size: 12px;' class="vamtam-button accent5  button-border hover-accent1 "><span class="btext" data-text="GET OFFER">GET OFFER</span></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
