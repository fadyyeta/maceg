﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MACEG.Default" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Home Page
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/custom.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&subset=latin-ext" rel="stylesheet">
    <%--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    --%>
    <%--fake--%>
    <style>
        .services-inside .thumbnail {
            background-color: initial;
        }

        /*.wpv-accordion li.pane-wrapper {
            border: none;
        }*/

        /*.wpv-accordion .tab:first-child {
            border-bottom: 0;
        }*/

        .wpv-accordion .tab.ui-state-hover, .wpv-accordion .tab.ui-state-active, .wpv-accordion .tab.ui-state-selected {
            background-color: white;
        }

        .glryCont {
            padding: 0 5px !important;
        }

        .partImg {
            height: 120px !important;
        }

        body ul li.partList {
            list-style-type: none;
            display: inline-table;
        }
    </style>
    <style>
        .lvPartners .carousel-item {
            width: 250px;
        }

        .lvPartners .thumb {
            width: 250px !important;
            display: -webkit-inline-box;
        }

        .lvPartners .img-fluid {
            height: 170px;
        }
    </style>
    <style>
        #demo {
            height: 100%;
            position: relative;
            overflow: hidden;
        }


        .green {
            background-color: #6fb936;
        }

        .thumb {
            margin-bottom: 20px;
        }

        .page-top {
            margin-top: 150px;
        }


        img.zoom {
            width: 100%;
            height: 200px;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }


        .transition {
            -webkit-transform: scale(1.2);
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }

        .modal-header {
            border-bottom: none;
        }

        .modal-title {
            color: #000;
        }

        .modal-footer {
            display: none;
        }
    </style>
    <%--slider for comments--%>
    <style>
        .yorum {
            display: flex;
            /*height: 100vh;*/
            align-items: center;
        }

        .card {
            border: none;
            box-shadow: 0 0 3px #0000001a;
        }

        .owl-item {
            padding: 10px;
        }

        .card-yazı p {
            padding: 30px 40px 50px 40px;
            font-family: 'Montserrat', sans-serif;
            font-weight: 500;
        }

            .card-yazı p::after {
                content: url("https://i.hizliresim.com/ODgyXA.png");
                width: 40px;
                height: 40px;
                position: absolute;
                margin: 10px 0 0 20px;
            }

            .card-yazı p::before {
                content: url("https://i.hizliresim.com/zMXJz4.png");
                width: 40px;
                height: 40px;
                position: relative;
                top: -10px;
                left: -18px;
            }

        .card-yazı {
            background-image: linear-gradient(135deg, rgb(255, 247, 32) 10%, rgb(60, 213, 0) 100%);
            position: relative;
            min-height: 300px;
            display: flex;
            align-items: center;
        }

        .favicon {
            width: 100%;
            height: auto;
        }

            .favicon img {
                height: 90px;
                width: 90px !important;
                position: absolute;
                bottom: -45px;
                left: 39%;
                border-radius: 100px;
                border: 8px solid white;
            }

        .puan {
            margin-top: 20px;
        }


            .puan img {
                height: 20px;
                width: 20px !important;
                margin: 30px 5px 0 0;
            }

        .isim h1 {
            font-size: 15px;
            font-family: 'Montserrat', sans-serif;
            font-weight: 700;
            margin-top: 20px;
        }

        .isim p {
            font-size: 12px;
            font-family: 'Montserrat', sans-serif;
            font-weight: 400;
            font-family: sans-serif;
        }
    </style>
    <%--slider styles--%>
    <style>
        body .carousel-item.slider-fullscreen-image img {
            min-height: 500px;
        }

        body .carousel-caption.justify-content-center {
            bottom: unset;
            top: 35%;
        }

        body .post-media-date .thumbnail {
            margin-bottom: 0 !important;
        }

        body .bx-prev, body .bx-next, body .nextPart, body .prevPart {
            margin-top: -26px;
            width: 50px;
            padding: 15px 12px;
            border-radius: 50%;
            top: 35%;
            background: #364352;
            color: #ededed;
            transition: all .3s;
            height: 250px;
            opacity: .8;
        }

        section.carousel {
            width: 100%;
            background-color: rgba(2,53,64,0.2) !important;
        }

        .slider-fullscreen-image {
            width: 100%;
            display: none;
            height: 500px;
        }

            .slider-fullscreen-image img {
                height: 100%;
                background-size: cover;
            }

            .slider-fullscreen-image .container-slide {
                width: 100%;
            }

            .slider-fullscreen-image.active {
                display: block;
            }

            .slider-fullscreen-image:after, .slider-fullscreen-image img:after {
                width: 100%;
                height: 100%;
                content: "";
                position: absolute;
                left: 0;
                top: 0;
                pointer-events: none;
                z-index: 2;
                background: rgba(255,255,255,0.2);
            }

        body .carousel-control-next {
            top: 50%;
            left: unset;
            right: 0;
            margin-right: 40px;
            height: 50px;
            border-radius: 100%;
            background-color: rgba(0,0,0,0.7);
            width: 50px;
            padding-top: 15px;
        }

        body .carousel-control-prev {
            top: 50%;
            right: unset;
            left: 0;
            margin-left: 40px;
            height: 50px;
            border-radius: 100%;
            background-color: rgba(0,0,0,0.7);
            width: 50px;
            padding-top: 15px;
        }

        .services-inside .thumbnail {
            border: initial;
        }

        p.mbr-text {
            color: #1E252E !important;
        }

        body .wwdtext {
            word-wrap: break-word;
            word-break: normal;
        }
    </style>
    <%--slider styles--%>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <header class="header-middle row type-slider">
        <div id="header-slider-container" class="revslider">
            <div class="header-slider-wrapper">
            </div>
        </div>
    </header>
    <header class="header-middle header-middle-bottom row normal type-featured" style="min-height: 0px">
        <div class="limit-wrapper">
            <div class="header-middle-content">
            </div>
        </div>
    </header>
    <div id="main-content">
        <div id="main" role="main" class="wpv-main layout-full">
            <div class="row page-wrapper">
                <section class="carousel slide cid-qWJRtMJ2Rr" data-interval="false" id="slider1-3">
                    <div class="full-screen">
                        <div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="false" data-interval="false">

                            <div class="carousel-inner" role="listbox">
                                <asp:ListView ID="lvSlider" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url('uploads/<%# Eval("url") %>'); background-size: cover">
                                            <div class="container container-slide">
                                                <div class="image_wrapper">
                                                    <div class="mbr-overlay"></div>
                                                    <div class="carousel-caption justify-content-center">
                                                        <div class="col-10 align-center">
                                                            <h2 class="mbr-fonts-style display-1">
                                                                <asp:Label Text='<%# Eval("name") %>' ID="lblTry" runat="server" /></h2>
                                                            <p class="lead mbr-text mbr-fonts-style display-5"><%# Eval("text") %></p>
                                                            <div class="mbr-section-btn" buttons="0"><a class="btn btn-success display-4" href="https://mobirise.com">FOR WINDOWS</a> <a class="btn  btn-info display-4" href="https://mobirise.com">FOR MAC</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                            <div data-app-prevent-settings="" id="carousel-control" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span><i class="glyphicon glyphicon-arrow-left"></i></div>
                            <div data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span><i class="glyphicon glyphicon-arrow-right"></i></div>
                        </div>
                    </div>

                </section>
                <article id="post-4" class="full post-4 page type-page status-publish hentry">
                    <div class="page-content">
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1 move-into-slider wpv-first-level first unextended has-extended-padding" style="padding-top: 0.05px; padding-bottom: 40px;" id="wpv-column-23287ed9abd4a431a5a0a4f284abfbee">
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-84128ecc6385ad4418e33393f131394a">
                                            <p>
                                                <img class="aligncenter size-full wp-image-9830" src="https://construction.vamtam.com/wp-content/uploads/2013/03/cask.png" alt="cask" width="180" height="140" srcset="https://construction.vamtam.com/wp-content/uploads/2013/03/cask.png 180w, https://construction.vamtam.com/wp-content/uploads/2013/03/cask-150x117.png 150w" sizes="(max-width: 180px) 100vw, 180px">
                                            </p>
                                            <h3 style="text-align: center;">We are intro… Tell the world <a href="#">about</a> yourself.</h3>
                                            <p style="text-align: center;">
                                                Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorperadipiscing elit, sed diam nonummy<br>
                                                nibh &nbsp;suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  wpv-first-level parallax-bg first has-background extended-content unextended has-extended-padding parallax-loaded" style="padding: 0.05px 15px;" id="wpv-column-a9dd695735abc3f176e7b4527238c980" data-parallax-method="to-centre" data-parallax-inertia="-0.3">
                                <div class="wpv-parallax-bg-img" style="background-position: 50% calc(50% - 180px); background-image: url(&quot;https://construction.vamtam.com/wp-content/uploads/2013/03/bg_services.jpg&quot;); background-color: rgb(199, 199, 199); background-size: cover; background-attachment: fixed; background-repeat: no-repeat;"></div>
                                <div class="wpv-parallax-bg-content">
                                    <div class="extended-column-inner">
                                        <style>
                                            #wpv-column-a9dd695735abc3f176e7b4527238c980 p, #wpv-column-a9dd695735abc3f176e7b4527238c980 em, #wpv-column-a9dd695735abc3f176e7b4527238c980 h1, #wpv-column-a9dd695735abc3f176e7b4527238c980 h2, #wpv-column-a9dd695735abc3f176e7b4527238c980 h3, #wpv-column-a9dd695735abc3f176e7b4527238c980 h4, #wpv-column-a9dd695735abc3f176e7b4527238c980 h5, #wpv-column-a9dd695735abc3f176e7b4527238c980 h6, #wpv-column-a9dd695735abc3f176e7b4527238c980 .column-title, #wpv-column-a9dd695735abc3f176e7b4527238c980 .sep-text h2.regular-title-wrapper, #wpv-column-a9dd695735abc3f176e7b4527238c980 .text-divider-double, #wpv-column-a9dd695735abc3f176e7b4527238c980 .sep-text .sep-text-line, #wpv-column-a9dd695735abc3f176e7b4527238c980 .sep, #wpv-column-a9dd695735abc3f176e7b4527238c980 .sep-2, #wpv-column-a9dd695735abc3f176e7b4527238c980 .sep-3, #wpv-column-a9dd695735abc3f176e7b4527238c980 td, #wpv-column-a9dd695735abc3f176e7b4527238c980 th, #wpv-column-a9dd695735abc3f176e7b4527238c980 caption {
                                                color: #484848;
                                            }

                                            #wpv-column-a9dd695735abc3f176e7b4527238c980:before {
                                                background-color: transparent;
                                            }
                                        </style>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first hide-bg-lowres has-background extended has-extended-padding" style="background: url( 'https://construction.vamtam.com/wp-content/uploads/2013/03/machine_bg.png' ) no-repeat center bottom; background-size: auto; background-attachment: scroll; padding-top: 0.05px; padding-bottom: 20px;"
                                                id="wpv-column-a95d8dc22982ae7cb6358525d6baf0e4">
                                                <div class="limit-wrapper">
                                                    <div class="extended-column-inner">
                                                        <h2 class="column-title">SERVICES</h2>
                                                        <div class="row ">
                                                            <div class="wpv-grid grid-1-5  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-b8379189bcdfa32883d3de64e2700f65">
                                                                <div class="services clearfix fullimage no-image has-icon has-button " style="text-align: center;">
                                                                    <div class="services-inside">
                                                                        <div class="thumbnail">
                                                                            <a href="/" title="General Builder" class="">
                                                                                <span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 58px !important;"></span> </a>
                                                                        </div>
                                                                        <div class="sep-2"></div>
                                                                        <h4 class="services-title">
                                                                            <a href="/" title="General Builder">General Builder</a>
                                                                        </h4>
                                                                        <div class="services-content">
                                                                            <p class="p1">
                                                                                <span class="s1">Mirum est notare quam littera gothica, quam nunc putamus parum claram.</span><br>
                                                                            </p>
                                                                        </div>
                                                                        <div class="services-button-wrap">
                                                                            <a href="/"><span class="btext">More »</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-2d3c9031c7abdee2aa52a6f26a50ebb6">
                                                                <div class="services clearfix fullimage no-image has-icon has-button " style="text-align: center;">
                                                                    <div class="services-inside">
                                                                        <div class="thumbnail">
                                                                            <a href="/" title="House Extensions" class="">
                                                                                <span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 58px !important;"></span> </a>
                                                                        </div>
                                                                        <div class="sep-2"></div>
                                                                        <h4 class="services-title">
                                                                            <a href="/" title="House Extensions">House Extensions</a>
                                                                        </h4>
                                                                        <div class="services-content">
                                                                            <p class="p1">
                                                                                <span class="s1">Mirum est notare quam littera gothica, quam nunc putamus parum claram.</span><br>
                                                                            </p>
                                                                        </div>
                                                                        <div class="services-button-wrap">
                                                                            <a href="/"><span class="btext">More »</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-fc8484aeaf699da69872e3c147f9fdbd">
                                                                <div class="services clearfix fullimage no-image has-icon has-button " style="text-align: center;">
                                                                    <div class="services-inside">
                                                                        <div class="thumbnail">
                                                                            <a href="/" title="Refurbishment" class="">
                                                                                <span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 58px !important;"></span> </a>
                                                                        </div>
                                                                        <div class="sep-2"></div>
                                                                        <h4 class="services-title">
                                                                            <a href="/" title="Refurbishment">Refurbishment</a>
                                                                        </h4>
                                                                        <div class="services-content">
                                                                            <p class="p1">
                                                                                <span class="s1">Mirum est notare quam littera gothica, quam nunc putamus parum claram.</span><br>
                                                                            </p>
                                                                        </div>
                                                                        <div class="services-button-wrap">
                                                                            <a href="/"><span class="btext">More »</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-8eef51c0c29b1534074c10a221deebc8">
                                                                <div class="services clearfix fullimage no-image has-icon has-button " style="text-align: center;">
                                                                    <div class="services-inside">
                                                                        <div class="thumbnail">
                                                                            <a href="/" title="Kitchens" class="">
                                                                                <span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 58px !important;"></span> </a>
                                                                        </div>
                                                                        <div class="sep-2"></div>
                                                                        <h4 class="services-title">
                                                                            <a href="/" title="Kitchens">Kitchens</a>
                                                                        </h4>
                                                                        <div class="services-content">
                                                                            <p class="p1">
                                                                                <span class="s1">Mirum est notare quam littera gothica, quam nunc putamus parum claram.</span><br>
                                                                            </p>
                                                                        </div>
                                                                        <div class="services-button-wrap">
                                                                            <a href="/"><span class="btext">More »</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="wpv-grid grid-1-5  unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-dc45318e4e6acd0b74afc247c0da7b36">
                                                                <div class="services clearfix fullimage no-image has-icon has-button " style="text-align: center;">
                                                                    <div class="services-inside">
                                                                        <div class="thumbnail">
                                                                            <a href="/" title="Electricity" class="">
                                                                                <span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 58px !important;"></span> </a>
                                                                        </div>
                                                                        <div class="sep-2"></div>
                                                                        <h4 class="services-title">
                                                                            <a href="/" title="Electricity">Electricity</a>
                                                                        </h4>
                                                                        <div class="services-content">
                                                                            <p class="p1">
                                                                                <span class="s1">Mirum est notare quam littera gothica, quam nunc putamus parum claram.</span><br>
                                                                            </p>
                                                                        </div>
                                                                        <div class="services-button-wrap">
                                                                            <a href="/"><span class="btext">More »</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row ">
                                                            <div class="wpv-grid grid-1-1  first unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-989bb485ddfd59519e383b152c238057">
                                                                <p class="textcenter"><a href="#" target="_self" style="font-size: 12px;" class="vamtam-button accent1  button-border hover-accent1 "><span class="btext" data-text="FULL LIST OF SERVICES »">FULL LIST OF SERVICES »</span></a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first hide-bg-lowres has-background unextended has-extended-padding" style="background: url( 'https://construction.vamtam.com/wp-content/uploads/2013/03/bg_about.jpg' ) no-repeat center bottom; background-size: auto; background-attachment: scroll; padding-top: 0.05px; padding-bottom: 0.05px;"
                                    id="wpv-column-59a51c1b881c9adadccbdbf794fc4d1c">
                                    <h2 class="text-divider-double">ABOUT </h2>
                                    <div class="sep"></div>
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-3  first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-d35080b684ca4502902ae1db65af2048">
                                            <p style="text-align: center;">
                                                <span class="icon shortcode theme  use-hover" style="font-size: 26px !important;"></span>
                                            </p>
                                            <h4 style="text-align: center;">Video presentation</h4>
                                            <div class="wpv-video-frame">
                                                <iframe width="1140" height="641" src="https://www.youtube.com/embed/V17Lp1X0_ao?feature=oembed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" style="height: 202.421px;"></iframe>
                                            </div>
                                            <p><a href="/about/">About</a> &nbsp;/&nbsp; <a href="/team/">Team</a> &nbsp;/&nbsp; <a href="/contact-us/">Contact</a></p>
                                            <p>Mauris accumsan eros eget libero posuere vulputate. Etiam elit elit, elementum sed varius at, adipiscing vitae est.</p>
                                            <p>Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum.</p>
                                            <p>
                                                Cost is important, so it is transparent.<br>
                                                You have constant access to the current spend and detailed breakdowns<br>
                                            </p>
                                            <div class="push " style="height: 30px"></div>
                                        </div>
                                        <div class="wpv-grid grid-1-3  unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-267ec32ebd1ed678c82420d7103ea77f">
                                            <p style="text-align: center;">
                                                <span class="icon shortcode theme  use-hover" style="font-size: 26px !important;"></span>
                                            </p>
                                            <h4 style="text-align: center;">Our services</h4>
                                            <p class="p1"><a href="#"><span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 16px !important;"></span><strong>&nbsp;GENERAL BUILDER</strong></a></p>
                                            <ul>
                                                <li>Extensions &amp; Loft conversions</li>
                                                <li>Refurbishment projects</li>
                                                <li>Electrical design with NIC electricians</li>
                                            </ul>
                                            <p class="p1"><a href="#"><span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 16px !important;"></span><strong>&nbsp;HOUSE EXTENSIONS</strong></a></p>
                                            <ul>
                                                <li>Quality Kitchens and bathrooms</li>
                                                <li>Automated and Sliding gate specialists</li>
                                                <li>Commercial refurbishments</li>
                                            </ul>
                                            <p class="p1"><a href="#"><span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 16px !important;"></span><strong>&nbsp;REFURBISHMENT </strong></a></p>
                                            <ul>
                                                <li>Garage conversions</li>
                                                <li>Hard &amp; Garden landscaping</li>
                                                <li>Roofing</li>
                                            </ul>
                                            <p class="p1"><a href="#"><span class="icon shortcode theme  use-hover" style="color: #F0542D; font-size: 16px !important;"></span><strong>&nbsp;KITCHENS &amp; BATHROOMS </strong></a></p>
                                            <ul>
                                                <li>Electrical design with NIC electricians</li>
                                                <li>Quality Kitchens and bathrooms</li>
                                            </ul>
                                            <div class="push " style="height: 30px"></div>
                                        </div>
                                        <div class="wpv-grid grid-1-3  unextended animation-from-bottom animated-active has-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-7821b318e3f1cb8dc31cde6800a237cb">
                                            <div class="row ">
                                                <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-5607b500bac355b53a7bf432c1d2d30b">
                                                    <p style="text-align: center;">
                                                        <span class="icon shortcode theme  use-hover" style="font-size: 26px !important;"></span>
                                                    </p>
                                                    <h4 style="text-align: center;">How we work?</h4>
                                                </div>
                                            </div>
                                            <div class="row ">
                                                <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-baedc4b9a769ac0ab25cf5fc9f943a27">
                                                    <div class="wpv-accordion-wrapper wpv-accordion-1">
                                                        <ul class="wpv-accordion ui-accordion ui-widget ui-helper-reset" data-collapsible="false" role="tablist">
                                                            <li class="pane-wrapper" style="">
                                                                <h4 class="tab ui-accordion-header ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons" role="tab" id="ui-id-1" aria-controls="ui-id-2" aria-selected="true" aria-expanded="true" tabindex="0"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span>
                                                                    <div class="inner">COST</div>
                                                                </h4>
                                                                <div class="pane ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" id="ui-id-2" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false" style="display: block;">
                                                                    <div class="inner">
                                                                        <div class="row ">
                                                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-e61e34720555a2beb7d563e821def485">
                                                                                <p>
                                                                                    <img class="alignnone size-full wp-image-9803" src="https://construction.vamtam.com/wp-content/uploads/2013/03/info-1.jpg" alt="info-1" width="320" height="150" srcset="https://construction.vamtam.com/wp-content/uploads/2013/03/info-1.jpg 320w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-1-150x70.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-1-262x123.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-1-150x70@2x.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">
                                                                                </p>
                                                                                <p>Cost is important, so it is transparent. You have constant access to the current spend and detailed breakdowns</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="pane-wrapper" style="">
                                                                <h4 class="tab ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-id-3" aria-controls="ui-id-4" aria-selected="false" aria-expanded="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
                                                                    <div class="inner">DURATION</div>
                                                                </h4>
                                                                <div class="pane ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-id-4" aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" style="display: none;">
                                                                    <div class="inner">
                                                                        <div class="row ">
                                                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-6a01759387884f42a58eefa52afc248f">
                                                                                <p>
                                                                                    <img class="alignnone size-full wp-image-9804" src="https://construction.vamtam.com/wp-content/uploads/2013/03/info-2.jpg" alt="info-2" width="320" height="150" srcset="https://construction.vamtam.com/wp-content/uploads/2013/03/info-2.jpg 320w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-2-150x70.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-2-262x123.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-2-150x70@2x.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">
                                                                                </p>
                                                                                <p>Cost is important, so it is transparent. You have constant access to the current spend and detailed breakdowns</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="pane-wrapper" style="">
                                                                <h4 class="tab ui-accordion-header ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-id-5" aria-controls="ui-id-6" aria-selected="false" aria-expanded="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
                                                                    <div class="inner">SCOPE OF WORKS</div>
                                                                </h4>
                                                                <div class="pane ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-id-6" aria-labelledby="ui-id-5" role="tabpanel" aria-hidden="true" style="display: none;">
                                                                    <div class="inner">
                                                                        <div class="row ">
                                                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-c4ebc68707e848d4cbb8165d66236b4c">
                                                                                <p>
                                                                                    <img class="alignnone size-full wp-image-9805" src="https://construction.vamtam.com/wp-content/uploads/2013/03/info-3.jpg" alt="info-3" width="320" height="150" srcset="https://construction.vamtam.com/wp-content/uploads/2013/03/info-3.jpg 320w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-3-150x70.jpg 150w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-3-262x123.jpg 262w, https://construction.vamtam.com/wp-content/uploads/2013/03/info-3-150x70@2x.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">
                                                                                </p>
                                                                                <p>Cost is important, so it is transparent. You have constant access to the current spend and detailed breakdowns</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <style>
                                                            .wpv-accordion-1 .tab .inner {
                                                                background-color: #ffffff;
                                                                color: #364352;
                                                            }
                                                        </style>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="push  wpv-hide-lowres" style="height: 30px"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  wpv-first-level first has-background extended-content unextended has-extended-padding" style="background: url( 'https://construction.vamtam.com/wp-content/uploads/2013/03/bg_gallery.jpg' ); background-size: cover; background-attachment: fixed; background-color: #808080; padding-top: 0.05px; padding-bottom: 30px; padding-left: 15px; padding-right: 15px;"
                                id="wpv-column-45ff610fee19b450d40d2ec7a4791ee4">
                                <div class="extended-column-inner">
                                    <style>
                                        #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 p, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 em, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 h1, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 h2, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 h3, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 h4, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 h5, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 h6, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 .column-title, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 .sep-text h2.regular-title-wrapper, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 .text-divider-double, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 .sep-text .sep-text-line, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 .sep, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 .sep-2, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 .sep-3, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 td, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 th, #wpv-column-45ff610fee19b450d40d2ec7a4791ee4 caption {
                                            color: #fff;
                                        }

                                        #wpv-column-45ff610fee19b450d40d2ec7a4791ee4:before {
                                            background-color: transparent;
                                        }
                                    </style>
                                    <h2 class="column-title">PROJECT GALLERY</h2>
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-f4bab7941b9227842384e376c6b2652d">
                                            <section class="portfolios isotope normal clearfix title-below has-description " id="5b9cdb5f1a037" style="height: auto; overflow: visible;">
                                                <nav class="sort_by_cat grid-1-1" data-for="#5b9cdb5f1a037" style="display: block;">
                                                    <span class="inner-wrapper">
                                                        <span class="cat"><a data-value="all" href="#" class="active">All</a></span>
                                                        <span class="cat"><a data-value="kitchens-bathrooms" href="#"><span data-text="BATHROOMS">BATHROOMS</span></a></span>
                                                        <span class="cat"><a data-value="general-builder" href="#"><span data-text="BUILDER">BUILDER</span></a></span>
                                                        <span class="cat"><a data-value="house-extensions" href="#"><span data-text="EXTENSIONS">EXTENSIONS</span></a></span>
                                                        <span class="cat" style="display: none;"><a data-value="gallery" href="#"><span data-text="GALLERY">GALLERY</span></a></span>
                                                        <span class="cat" style="display: none;"><a data-value="html" href="#"><span data-text="HTML">HTML</span></a></span>
                                                        <span class="cat"><a data-value="image-link" href="#"><span data-text="IMAGE">IMAGE</span></a></span>
                                                        <span class="cat"><a data-value="portrait" href="#"><span data-text="PORTRAIT">PORTRAIT</span></a></span>
                                                        <span class="cat"><a data-value="refurbishment" href="#"><span data-text="REFURBISHMENT">REFURBISHMENT</span></a></span>
                                                        <span class="cat"><a data-value="video" href="#"><span data-text="VIDEO">VIDEO</span></a></span>
                                                    </span>
                                                </nav>
                                                <div class="row glryCont">


                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://images.pexels.com/photos/62307/air-bubbles-diving-underwater-blow-62307.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                                                            <img src="https://images.pexels.com/photos/62307/air-bubbles-diving-underwater-blow-62307.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid " alt="">
                                                        </a>
                                                    </div>
                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://uniquefacts.net/wp-content/uploads/2015/09/Technology.jpeg" class="fancybox" rel="ligthbox">
                                                            <img src="https://uniquefacts.net/wp-content/uploads/2015/09/Technology.jpeg" class="zoom img-fluid" alt="">
                                                        </a>
                                                    </div>

                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://images.pexels.com/photos/158827/field-corn-air-frisch-158827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="fancybox" rel="ligthbox">
                                                            <img src="https://images.pexels.com/photos/158827/field-corn-air-frisch-158827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="zoom img-fluid " alt="">
                                                        </a>
                                                    </div>

                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://images.pexels.com/photos/302804/pexels-photo-302804.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="fancybox" rel="ligthbox">
                                                            <img src="https://images.pexels.com/photos/302804/pexels-photo-302804.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="zoom img-fluid " alt="">
                                                        </a>
                                                    </div>

                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://images.pexels.com/photos/1038914/pexels-photo-1038914.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                                                            <img src="https://images.pexels.com/photos/1038914/pexels-photo-1038914.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid " alt="">
                                                        </a>
                                                    </div>

                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://images.pexels.com/photos/414645/pexels-photo-414645.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="fancybox" rel="ligthbox">
                                                            <img src="https://images.pexels.com/photos/414645/pexels-photo-414645.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" class="zoom img-fluid " alt="">
                                                        </a>
                                                    </div>

                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://image.shutterstock.com/image-vector/vector-illustration-galaxy-fantasy-background-260nw-1055265833.jpg" class="fancybox" rel="ligthbox">
                                                            <img src="https://image.shutterstock.com/image-vector/vector-illustration-galaxy-fantasy-background-260nw-1055265833.jpg" class="zoom img-fluid " alt="">
                                                        </a>
                                                    </div>

                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                        <a href="https://images.pexels.com/photos/1038002/pexels-photo-1038002.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="fancybox" rel="ligthbox">
                                                            <img src="https://images.pexels.com/photos/1038002/pexels-photo-1038002.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="zoom img-fluid " alt="">
                                                        </a>
                                                    </div>




                                                </div>

                                            </section>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="wpv-grid grid-1-1  first unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-5f49d4283977a35262abcd844612f990">
                                            <div class="push " style="height: 30px"></div>
                                            <p class="textcenter"><a href="/portfolio-layouts/masonry-style/" target="_self" style="font-size: 12px;" class="vamtam-button accent1  button-border hover-accent1 "><span class="btext" data-text="VIEW FULL GALLERY »">VIEW FULL GALLERY »</span></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="push " style="height: 30px"></div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended animation-fade-in animated-active has-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-162f5bffa4ba6f8bd0431143a4a59c08">
                                    <div class="row ">
                                        <div class="col-sm-12 ">
                                            <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="9000">
                                                <div class="lvPartners carousel-inner row w-100 mx-auto" role="listbox" style="max-height: 160px; display: -webkit-inline-box;">
                                                    <marquee loop="true" height="170px" style="border: none;" onmouseover="this.stop()" onmouseout="this.start()">
                                                        <asp:ListView ID="lvPartners" ClientIDMode="Static" runat="server">
                                                            <ItemTemplate>
                                                                <a title="<%# Eval("name") %>" class="thumb">
                                                                    <img class="img-fluid mx-auto d-block img-responsive" src="uploads/<%# Eval("imageUrl") %>" alt="<%# Eval("name") %>">
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </marquee>
                                                </div>
                                                <%--<a class="carousel-control-prev prevPart" href="#carouselExample" role="button" data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                                </a>
                                                <a class="carousel-control-next text-faded nextPart" href="#carouselExample" role="button" data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                                </a>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="push " style="height: 30px"></div>
                        <div class="row ">
                            <div class="wpv-grid grid-1-1  wpv-first-level first has-background extended animation-from-bottom animated-active has-extended-padding animation-ended" style="background: url( 'https://construction.vamtam.com/wp-content/uploads/2013/03/testimonilas_bg.jpg' ) no-repeat center center; background-size: cover; background-attachment: scroll; background-color: #3B6097; padding-top: 140px; padding-bottom: 40px;"
                                id="wpv-column-2c5857472d48efb77a143b6c512e889b">
                                <div class="limit-wrapper">
                                    <div class="extended-column-inner">
                                        <style>
                                            #wpv-column-2c5857472d48efb77a143b6c512e889b p, #wpv-column-2c5857472d48efb77a143b6c512e889b em, #wpv-column-2c5857472d48efb77a143b6c512e889b h1, #wpv-column-2c5857472d48efb77a143b6c512e889b h2, #wpv-column-2c5857472d48efb77a143b6c512e889b h3, #wpv-column-2c5857472d48efb77a143b6c512e889b h4, #wpv-column-2c5857472d48efb77a143b6c512e889b h5, #wpv-column-2c5857472d48efb77a143b6c512e889b h6, #wpv-column-2c5857472d48efb77a143b6c512e889b .column-title, #wpv-column-2c5857472d48efb77a143b6c512e889b .sep-text h2.regular-title-wrapper, #wpv-column-2c5857472d48efb77a143b6c512e889b .text-divider-double, #wpv-column-2c5857472d48efb77a143b6c512e889b .sep-text .sep-text-line, #wpv-column-2c5857472d48efb77a143b6c512e889b .sep, #wpv-column-2c5857472d48efb77a143b6c512e889b .sep-2, #wpv-column-2c5857472d48efb77a143b6c512e889b .sep-3, #wpv-column-2c5857472d48efb77a143b6c512e889b td, #wpv-column-2c5857472d48efb77a143b6c512e889b th, #wpv-column-2c5857472d48efb77a143b6c512e889b caption {
                                                color: #fff;
                                            }

                                            #wpv-column-2c5857472d48efb77a143b6c512e889b:before {
                                                background-color: transparent;
                                            }
                                        </style>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-084cd51c7b9226c1279f255b5f862e1e">
                                                <p style="text-align: center;">
                                                    <span class="icon shortcode theme  use-hover" style="color: #ffffff; font-size: 60px !important;"></span>
                                                </p>
                                                <h2 style="text-align: center;">CUSTOMER REVIEWS</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="push  wpv-hide-lowres" style="margin-bottom: -35px"></div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <section class="yorum">
                                    <div class="container">
                                        <div class="col-12">
                                            <div class="owl-carousel">
                                                <asp:ListView ID="lvReviews" ClientIDMode="Static" OnItemDataBound="lvReviews_ItemDataBound" runat="server">
                                                    <ItemTemplate>
                                                        <div class="">
                                                            <div class="wpv-grid col-md-12  wpv-first-level first unextended animation-from-bottom animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-a83a984c1b394bcd5bef9bf5dcf240bd">
                                                                <div class="blockquote-list">
                                                                    <blockquote class="clearfix small simple post-9665 testimonials type-testimonials status-publish hentry testimonials_category-slider-no-photo">
                                                                        <h3 class="quote-summary">"<%# Eval("revTitle") %>"</h3>
                                                                        <div class="quote-title-wrapper clearfix">
                                                                            <div class="quote-title">
                                                                                <asp:HiddenField ID="hfStars" ClientIDMode="Static" Value='<%# Eval("revStars") %>' runat="server" />
                                                                                <asp:ListView ID="lvRating" ClientIDMode="Static" runat="server">
                                                                                    <ItemTemplate>
                                                                                        <span class="icon shortcode   use-hover" style="color: #F8DF04;"></span>
                                                                                    </ItemTemplate>
                                                                                </asp:ListView>
                                                                                — <span class="the-title"><%# Eval("revName") %></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="quote-text">
                                                                            <div class="quote-content">
                                                                                <p><%# Eval("revComment") %></p>
                                                                            </div>
                                                                        </div>
                                                                    </blockquote>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row ">
                                <div class="wpv-grid grid-1-1  wpv-first-level first unextended animation-fade-in animated-active no-extended-padding animation-ended" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-9b2eec77dcf7943570842416086b0f8f">
                                    <h2 class="text-divider-double">
                                        <span class="icon shortcode theme  use-hover" style="color: #364352; font-size: 26px !important;"></span> LATEST NEWS </h2>
                                    <div class="sep"></div>
                                </div>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                            <div class="row page-wrapper">
                                <article id="post-277" class="full post-277 page type-page status-publish hentry">
                                    <div class="page-content">
                                        <div class="push  wpv-hide-lowres" style="height: 40px"></div>
                                        <div class="row ">
                                            <div class="wpv-grid grid-1-1  wpv-first-level first unextended no-extended-padding" style="padding-top: 0.05px; padding-bottom: 0.05px;" id="wpv-column-b6b0a3c50843021a3835ad61f3322943">
                                                <div class="loop-wrapper clearfix news row masonry paginated wpv-isotope-loaded" data-columns="3" style="position: relative;">
                                                    <div class="table">
                                                        <ul class="horizontal-list" style="list-style-type: none;">
                                                            <asp:ListView ID="lvNewssList" ClientIDMode="Static" runat="server">
                                                                <ItemTemplate>
                                                                    <li class="page-content post-header list-item post-304 post type-post status-publish format-image has-post-thumbnail hentry category-car-tuning post_format-post-format-image isotope-item col-sm-6 col-xs-12">
                                                                        <div class="newsWrapper">
                                                                            <div class="post-article has-image-wrapper ">
                                                                                <div class="image-post-format clearfix as-normal ">
                                                                                    <div class="post-media-date">
                                                                                        <div class="thumbnail">
                                                                                            <a href="NewsData.aspx?ID=<%# Eval("ID") %>" title="<%# Eval("name") %>">
                                                                                                <img src="uploads/<%# Eval("urlImage") %>" class="attachment-theme-normal-3 size-theme-normal-3 wp-post-image img-responsive" style="max-height: 300px;" alt="<%# Eval("name") %>">
                                                                                                <span class="icon shortcode theme  use-hover" style=""></span> </a>
                                                                                        </div>
                                                                                        <div class="post-actions-wrapper clearfix">
                                                                                            <div class="post-date">
                                                                                                <%# Eval("date", "{0:d MMM, yyyy}") %>
                                                                                            </div>
                                                                                            <div class="comment-count">
                                                                                                <a href="NewsData.aspx?ID=<%# Eval("ID") %>"><span class="icon"></span><asp:Literal Text="0" ID="lblCommentCount" runat="server" />
                                                                                                    <span class="comment-word visuallyhidden">Comments</span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="post-content-wrapper">
                                                                                        <header class="single">
                                                                                            <div class="content">
                                                                                                <h3>
                                                                                                    <a href="NewsData.aspx?ID=<%# Eval("ID") %>" title="<%# Eval("name") %>" class="entry-title"><%# Eval("name") %></a>
                                                                                                </h3>
                                                                                            </div>
                                                                                        </header>
                                                                                        <div class="post-content-outer">
                                                                                            <p class="wwdtext"><%# Eval("text") %></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:ListView>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="push  wpv-hide-lowres" style="height: 50px"></div>
                                    </div>
                                    <div class="limit-wrapper">
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="push  wpv-hide-lowres" style="height: 40px"></div>

                    </div>
                    <div class="limit-wrapper">
                    </div>
                </article>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".lvPartners.carousel-inner:first").addClass("active");


            $(".carousel-item.slider-fullscreen-image:nth-child(1)").addClass('active');
        });
    </script>
    <script>
        $(document).ready(function () {
            autoSlide();

            $('.wwdtext').each(function (ind, elem) {
                var newtext = $(elem).text($(elem).text().substring(0, 100)).text();
                newtext += "<span>... Read More</span>";
                $(elem).html(newtext);
            });


        });
        function autoSlide() {
            //var activeSlide = $(".slider-fullscreen-image.active");
            var tid = setInterval(showNext, 10000);

        }
        function showNext() {
            var activeSlide = $(".slider-fullscreen-image.active");
            if ($(".slider-fullscreen-image:last").hasClass("active")) {
                $(".slider-fullscreen-image").removeClass("active");
                $(".slider-fullscreen-image:first").addClass("active");

            }
            else {
                $(".slider-fullscreen-image").removeClass("active");
                activeSlide.next().addClass("active");
            }
        }
        function showPrev() {
            var activeSlide = $(".slider-fullscreen-image.active");
            if ($(".slider-fullscreen-image:first").hasClass("active")) {
                $(".slider-fullscreen-image").removeClass("active");
                $(".slider-fullscreen-image:last").addClass("active");

            }
            else {
                $(".slider-fullscreen-image").removeClass("active");
                activeSlide.prev().addClass("active");
            }
        }
        $(".carousel-control").click(function () {
            var activeSlide = $(".slider-fullscreen-image.active");
            if ($(this).hasClass("carousel-control-next")) {
                showNext();
            }
            else {
                showPrev();
            }
        });
        //ui-accordion-header-icon ui-icon ui-icon-triangle-1-e ...... plus
        //ui-accordion-header-icon ui-icon ui-icon-triangle-1-s ...... minus
        //pane ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active ...... active
        //pane ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom .. inactive
        $(".ui-accordion-header-icon").click(function () {
            var thisItem = $(this);
            $(".ui-accordion-header-icon").removeClass("ui-icon-triangle-1-s");
            $(".ui-accordion-header-icon:after").css("display", "none");
            $(".ui-accordion-header-icon").addClass("ui-icon-triangle-1-e");
            $(thisItem).closest(".ui-accordion-header-icon").removeClass("ui-icon-triangle-1-e").addClass("ui-icon-triangle-1-s");
            $(".ui-accordion-content").removeClass("ui-accordion-content-active");
            $(".ui-accordion-content").css("display", "none");
            var containerList = $(thisItem).closest("li.pane-wrapper");
            var paneSelected = $(containerList).find(".ui-accordion-content");
            $(paneSelected).addClass("ui-accordion-content-active");
            $(paneSelected).css("display", "block");
        });
    </script>
    <script>

        $('#carouselExample').on('slide.bs.carousel', function (e) {


            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 4;
            var totalItems = $('.carousel-item').length;

            if (idx >= totalItems - (itemsPerSlide - 1)) {
                var it = itemsPerSlide - (totalItems - idx);
                for (var i = 0; i < it; i++) {
                    // append slides to end
                    if (e.direction == "left") {
                        $('.carousel-item').eq(i).appendTo('.carousel-inner');
                    }
                    else {
                        $('.carousel-item').eq(0).appendTo('.carousel-inner');
                    }
                }
            }
        });


        $('#carouselExample').carousel({
            interval: 200
        });


        $(document).ready(function () {
            /* show lightbox when clicking a thumbnail */
            $('a.thumb').click(function (event) {
                event.preventDefault();
                var content = $('.modal-body');
                content.empty();
                var title = $(this).attr("title");
                $('.modal-title').html(title);
                content.html($(this).html());
                $(".modal-profile").modal({ show: true });
            });
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                        }
                        //,
                    //1000: {
                    //    items: 3
                    //}
                }
            });
        });
    </script>
</asp:Content>
