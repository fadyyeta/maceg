﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="cart.aspx.cs" Inherits="MACEG.cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Cart
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        body .title {
            padding: 0 0 20px !important;
        }

        table.shop_table {
            border: none !important;
        }

            table.shop_table tbody:last-of-type {
                border: none !important;
            }

            table.shop_table thead, table.shop_table tbody {
                border-bottom: 1px solid gray !important;
            }

        #main {
            margin: 40px;
        }

        .cart_totals > * {
            width: 100%;
        }
        .option-box {
            font-size: 25pt;
            /*background-color : #cecece ;*/
            margin: auto;
            border: solid 3px #3B6097;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div id="main-content">
        <div id="sub-header" class="layout-full has-background">
            <div class="meta-header" style="">
                <div class="limit-wrapper">
                    <div class="meta-header-inside">
                        <header class="page-header ">
                            <div class="page-header-content">
                                <h1 style="">
                                    <span class="title">
                                        <span itemprop="headline">
                                            <span class="checkout-breadcrumb">
                                                <span class="title-part-active">My Cart</span>
                                            </span>
                                        </span>
                                    </span>
                                </h1>
                            </div>
                        </header>
                    </div>
                </div>
            </div>
        </div>
        <div id="main" role="main" class="wpv-main layout-full">
            <div class="limit-wrapper">
                <div class="row page-wrapper">
                    <article id="post-9699" class="full post-9699 page type-page status-publish hentry">
                        <div class="page-content">
                            <div class="push  wpv-hide-lowres" style="height: 30px"></div>
                            <div class="woocommerce">
                                <form class="woocommerce-cart-form" action="https://construction.vamtam.com/cart/" method="post">
                                    <div class="row">
                                        <div class="vamtam-grid grid-2-3">
                                            <asp:Panel ID="cartContent" ClientIDMode="Static" runat="server">
                                            </asp:Panel>
                                            <div id="tblCart" runat="server">
                                                <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th class="product-thumbnail">&nbsp;</th>
                                                            <th class="product-name">Product</th>
                                                            <th class="product-price">Price</th>
                                                            <th class="product-quantity">Quantity</th>
                                                            <th class="product-subtotal">Total</th>
                                                            <th class="product-remove">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:ListView ID="lvOrders" ClientIDMode="Static" runat="server">
                                                            <ItemTemplate>
                                                                <tr class="woocommerce-cart-form__cart-item cart_item">
                                                                    <td class="product-thumbnail">
                                                                        <a href="#">
                                                                            <img width="50" height="50" src="uploads/<%# Eval("imageUrl") %>"></a> </td>
                                                                    <td class="product-name" data-title="Product">
                                                                        <a href="#"><%# Eval("prodName") %></a> </td>
                                                                    <td class="product-price" data-title="Price">
                                                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span><%# Eval("Price") %></span> </td>
                                                                    <td class="product-quantity" data-title="Quantity"><%# Eval("Qty") %>
                                                                        <input type="hidden" name="cart[47ce0875420b2dbacfc5535f94e68433][qty]" value="1">
                                                                    </td>
                                                                    <td class="product-subtotal" data-title="Total">
                                                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span><%# Eval("total") %></span> </td>
                                                                    <td class="product-remove">
                                                                        <a href="#" class="remove" aria-label="Remove this item" data-product_id="8994" data-product_sku="458">×</a> </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="vamtam-grid grid-1-3">
                                            <div class="cart-collaterals">
                                                <div class="cart_totals ">
                                                    <h2>Cart totals</h2>
                                                    <table cellspacing="0">
                                                        <tbody>
                                                            <tr class="cart-subtotal">
                                                                <th>Subtotal</th>
                                                                <td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>4.90</span></td>
                                                            </tr>
                                                            <tr class="order-total">
                                                                <th>Total</th>
                                                                <td><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">£</span>4.90</span></strong> </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="wc-proceed-to-checkout">
                                                        <input type="submit" class="button" name="update_cart" value="Update cart" disabled="">
                                                        <a href="https://construction.vamtam.com/checkout/" class="checkout-button button alt wc-forward">Proceed to checkout</a>
                                                    </div>
                                                </div>
                                                <div class="cart-contents">
                                                    <div class="coupon">
                                                        <label for="coupon_code">Coupon:</label>
                                                        <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code">
                                                        <input type="submit" name="apply_coupon" value="Apply Coupon">
                                                    </div>
                                                    <input type="hidden" id="_wpnonce" name="_wpnonce" value="89d167cebe"><input type="hidden" name="_wp_http_referer" value="/cart/">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="limit-wrapper">
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
