﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace MACEG
{
    public partial class Master : System.Web.UI.MasterPage
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadFData();
            }
        }
        protected void loadFData()
        {
            DataSet ds = codeClass.SQLREAD("select top (1) * from FixedData");
            lblSchedule.Text = ds.Tables[0].Rows[0]["schedule"].ToString();
            YearLabel.Text = DateTime.Now.Year.ToString();
            if (Session["accType"] == null)
            {
                Session["accType"] = "none";
            }
            if (Session["accType"].ToString() == "Admin")
            {
                AdminSpan.Visible = true;
            }
            else
            {
                AdminSpan.Visible = false;
            }
        }
    }
}