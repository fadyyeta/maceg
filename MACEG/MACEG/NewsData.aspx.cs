﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MACEG
{
    public partial class NewsData : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadFields();
                loadRelated();
                loadAside();
                loadArticle();
            }
        }
        protected void loadRelated()
        {
            DataSet ds = codeClass.SQLREAD("select * from Product");
            lvRelated.DataSource = ds;
            lvRelated.DataBind();
        }
        protected void loadAside()
        {
            DataSet dsCat = codeClass.SQLREAD("select * from Category");
            lvCat.DataSource = dsCat;
            lvCat.DataBind();
            DataSet dsTags = codeClass.SQLREAD("select * from Tags");
            lvTags.DataSource = dsTags;
            lvTags.DataBind();
        }
        protected void loadArticle()
        {
            string id = Request.QueryString["ID"];
            DataSet ds = codeClass.SQLREAD("select *, (select count(ID)  from comments where postID = " + id + " And postType = N'News') as county from News where ID = " + id);
            lvArticle.DataSource = ds;
            lvArticle.DataBind();
            DataSet dsCom = codeClass.SQLREAD("select *  from comments where postID = "+id+" And postType = N'News'");
            lvComments.DataSource = dsCom;
            lvComments.DataBind();

        }
        protected void loadFields()
        {
            string sessUser;
            if (Session["userID"] == null)
            {
                Session["userID"] = "";
                sessUser = "";
            }
            else
            {
                sessUser = Session["userID"].ToString();
            }
            if (sessUser != null && sessUser.Trim() != "")
            {
                DataSet ds = codeClass.SQLREAD("select * from Users where ID = " + sessUser);
                if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
                {
                    return;
                }
                else
                {
                    autherCont.Visible = false;
                    mailCont.Visible = false;
                }

            }
        }

        protected void btnComment_Click(object sender, EventArgs e)
        {
            string name;

            string message = comment.Text;

            string emaily;
            string sessUser;
            if (Session["userID"] == null)
            {
                Session["userID"] = "";
                sessUser = "";
            }
            else
            {
                sessUser = Session["userID"].ToString();
            }
            if (sessUser == "")
            {

                name = author.Value;
                emaily = email.Value;
                codeClass.SQLINSERT("insert into comments (comment,postID, postType, guestName, guestMail) values (N'"+message +"',"+int.Parse(Request.QueryString["ID"].ToString())+",N'News',N'"+ name + "',N'"+ emaily + "')");
            }
            if (sessUser != "")
            {
                DataSet ds = codeClass.SQLREAD("select * from Users where ID = " + sessUser);
                string ppUrl = ds.Tables[0].Rows[0]["ppUrl"].ToString();
                string uName = ds.Tables[0].Rows[0]["userName"].ToString();
                string uMail = ds.Tables[0].Rows[0]["email"].ToString();
                codeClass.SQLINSERT("insert into comments (comment,postID, postType,userID, guestName, guestMail, imgUrl) values (N'" + message + "'," + int.Parse(Request.QueryString["ID"].ToString()) + ",N'News',"+sessUser+",N'" + uName + "',N'" + uMail + "',N'"+ ppUrl + "' )");

            }


        }
    }
}