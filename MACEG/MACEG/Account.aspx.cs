﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MACEG
{
    public partial class Account : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Abandon();
            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string user = txtUser.Text;
            string pass = txtPass.Text;
            login(user, pass);

        }
        protected void login(string user, string pass)
        {
            DataSet ds = codeClass.SQLREAD("select * from Users where email = N'" + user + "' and passwordy = N'" + pass + "'");
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {
                return;
            }
            else
            {
                Session["userID"] = ds.Tables[0].Rows[0]["ID"].ToString();
                DataTable dt = ds.Tables[0];
                if (dt.Rows[0]["accType"].ToString() == "Admin")
                {
                    Session["accType"] = "Admin";
                    Response.Redirect("App.aspx?XID=XML");
                }
                else
                {
                    Session["accType"] = dt.Rows[0]["accType"].ToString();
                    Response.Redirect("Default.aspx");
                }
                Session.Timeout = 10080;
            }
        }
        protected void SendMail()
        {
            DataSet ds = codeClass.SQLREAD("select * from Users where email = '" + email.Value+"'");
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {

            }
            else
            {
                string hashXC = Guid.NewGuid().ToString();
                string linkRes = " www.macegy.apphb.com/passReset.aspx?XCode="+ hashXC;
                MailMessage message = new MailMessage();
                message.Subject = "Password Reset for M.A.C. Egypt Account";
                string body = "Sorry for the Inconvinious, to reset your password please follow the below link : \n ";
                body += linkRes;
                message.Body = body;
                string mailFrom = email.Value;
                message.From = new MailAddress(mailFrom);
                var fromAddress = "fady.yeta@gmail.com";
                //message.To = 


                //Password of your mail address
                //const string fromPassword = "D@milestone3";
                const string fromPassword = "An@F@dy2010";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                smtp.Send(mailFrom, fromAddress, "Password Reset", body);
                codeClass.SQLINSERT("Insert into passResett (email, hashedy) values (N'"+ email.Value + "' , N'" + hashXC + "')");

            }


        }
        protected void btnReg_Click(object sender, EventArgs e)
        {
            int commandID = codeClass.SQLGetLastID("Insert INTO Users (fName,lName,userName, email, passwordy, phone, accType) values (N'" + txtfNm.Text + "' , N'" + txtLNm.Text + "' , N'" + txtUserName.Text + "' , N'" + txtEmailReg.Text + "' , N'" + txtPassReg.Text + "' , N'" + txtPhone.Value + "' , N'" + accTyp.Items[accTyp.SelectedIndex].Text + "')  SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]");
            DataSet ds = codeClass.SQLREAD("select * from Users where ID = " + commandID);
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {
                return;
            }
            else
            {
                string user = ds.Tables[0].Rows[0]["email"].ToString();
                string pass = ds.Tables[0].Rows[0]["passwordy"].ToString();
                login(user, pass);
            }
        }

        protected void btnForgot_ServerClick(object sender, EventArgs e)
        {
            SendMail();
        }
    }
}