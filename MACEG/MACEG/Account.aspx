﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="MACEG.Account" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Account | M.A.C. Egypt</title>
    <link href="http://www.mac-egypt.com/images/logo.png" rel="icon" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap_css">
    <link href="CSS/accountStyles.css" rel="stylesheet" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .anchorBtn {
            background-color: white;
            line-height: 25pt;
            font-size: 15pt;
            padding: 10px;
            border-radius: 25px;
        }

            .anchorBtn:hover {
                text-decoration: none;
            }

        .ForgetPwd, .btnSubmit {
            padding: 10px 20px;
            border-radius: 40px;
        }

        #passResetModal {
            display: none;
            position: absolute;
            top: 150px;
            margin: auto;
            width: 100%;
            z-index: 3;
            left: 0;
        }

        .overlayBG {
            background-color: rgba(0,0,0,0.4);
            z-index: 2;
            height: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <div class="container-fluid register">
                <div class="row">
                    <!-- Modal -->
                    <div id="passResetModal">
                        <div class="overlayBG">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="text-center">
                                                            <h3><i class="fa fa-lock fa-4x"></i></h3>
                                                            <h2 class="text-center">Forgot Password?</h2>
                                                            <p>You can reset your password here.</p>
                                                            <div class="panel-body">

                                                                <form id="register-form" role="form" autocomplete="off" class="form" method="post">

                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                                            <input id="email" runat="server" name="email" placeholder="email address" class="form-control" type="email">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit"  id="btnForgot" runat="server" onserverclick="btnForgot_ServerClick">
                                                                    </div>

                                                                    <input type="hidden" class="hide" name="token" id="token" value="">
                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="close btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt="" />
                        <h3>Welcome</h3>
                        <p>You are 30 seconds away from getting members-only discounts!</p>
                        <a href="Default.aspx" class="anchorBtn">Back To Website</a><br />
                    </div>
                    <div class="col-md-9 register-right">
                        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="Regi-tab" data-toggle="tab" href="#" role="tab" aria-controls="home" aria-selected="true">Register</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="Sign-tab" data-toggle="tab" href="#" role="tab" aria-controls="profile" aria-selected="false">Sign In</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show" id="RegisterPnl" role="tabpanel" aria-labelledby="home-tab">
                                <h3 class="register-heading">Register to earn our members-only features</h3>
                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:TextBox runat="server" CssClass="form-control" placeholder="First Name *" ID="txtfNm" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox runat="server" CssClass="form-control" placeholder="Last Name *" ID="txtLNm" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox TextMode="Password" runat="server" CssClass="form-control" placeholder="Password *" ID="txtPassReg" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox TextMode="Password" runat="server" CssClass="form-control" placeholder="Confirm Password *" ID="txtPassConf" />
                                        </div>
                                        <%--<div class="form-group">
                                            <div class="maxl">
                                                <label class="radio inline">
                                                    <input type="radio" name="gender" value="male" checked>
                                                    <span>Male </span>
                                                </label>
                                                <label class="radio inline">
                                                    <input type="radio" name="gender" value="female">
                                                    <span>Female </span>
                                                </label>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:TextBox runat="server" CssClass="form-control" placeholder="Email Address *" ID="txtEmailReg" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox runat="server" CssClass="form-control" placeholder="User Name *" ID="txtUserName" />
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" ClientIDMode="Static" id="accTyp" runat="server">
                                                <option class="hidden" selected disabled>Please select your account type</option>
                                                <option value="Admin">Admin</option>
                                                <option value="B2B">Business Representative (B2B)</option>
                                                <option value="B2C">Consumer (B2C)</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text"  name="txtEmpPhone" class="form-control" placeholder="Your Phone *" value="" runat="server" id="txtPhone" />
                                        </div>
                                        <asp:Button Text="Register" ID="btnReg" CssClass="btnRegister" OnClick="btnReg_Click" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show active" id="LoginPnl" role="tabpanel" aria-labelledby="profile-tab">
                                <h3 class="register-heading">Sign in to enjoy your features</h3>
                                <div class="row register-form">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:TextBox CssClass="form-control" placeholder="Your Email *" ID="txtUser" runat="server" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox CssClass="form-control" TextMode="Password" placeholder="Your Password *" ID="txtPass" runat="server" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Button Text="Login" CssClass="btn btnSubmit" ID="btnLogin" OnClick="btnLogin_Click" runat="server" />
                                            <button class="ForgetPwd btn btn-info">Forgot Password?</button>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script>
            $("#Regi-tab").click(function () {
                $("#Regi-tab").addClass("active");
                $("#RegisterPnl").addClass("active");
                $("#Sign-tab").removeClass("active");
                $("#LoginPnl").removeClass("active");
            });
            $("#Sign-tab").click(function () {
                $("#Sign-tab").addClass("active");
                $("#LoginPnl").addClass("active");
                $("#Regi-tab").removeClass("active");
                $("#RegisterPnl").removeClass("active");
            });
            $(".ForgetPwd").click(function () {
                $("#passResetModal").css('display', 'inline-grid');

                return false;
            });
            $("button.close").click(function () {
                $("#passResetModal").css('display', 'none');

                return false;
            });
        </script>
    </form>
</body>
</html>
