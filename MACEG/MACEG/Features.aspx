﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Features.aspx.cs" Inherits="MACEG.Features" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Users only features
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        /*============ Service Features style ============*/
        .service-heading-block {
            display: block;
            margin-bottom: 30px;
        }

        .title {
            display: block;
            font-size: 30px;
            font-weight: 200;
            margin-bottom: 10px;
        }

        .sub-title {
            font-size: 18px;
            font-weight: 100;
            line-height: 24px;
        }

        .feature-block {
            background-color: #fff;
            border-radius: 2px;
            padding: 15px;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
            margin-bottom: 15px;
            transition: all ease .5s;
        }

        .ms-feature:hover,
        .ms-feature:focus {
            background-color: #fafafa;
            box-shadow: 0 3px 4px 3px rgba(0, 0, 0, 0.14), 0 3px 3px -2px rgba(0, 0, 0, 0.2), 0 1px 8px 3px rgba(0, 0, 0, 0.12);
        }

        .fb-icon {
            border-radius: 50%;
            display: block;
            font-size: 36px;
            height: 80px;
            line-height: 80px;
            text-align: center;
            margin: 1rem auto;
            width: 80px;
            transition: all 0.5s ease 0s;
        }

        .feature-block:hover .fb-icon,
        .feature-block:focus .fb-icon {
            box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.2);
            transform: rotate(360deg);
        }

        .fb-icon.color-info {
            background-color: #5bc0de;
            color: #fff;
        }

        .fb-icon.color-warning {
            background-color: #eea236;
            color: #fff;
        }

        .fb-icon.color-success {
            background-color: #5cb85c;
            color: #fff;
        }

        .fb-icon.color-danger {
            background-color: #d9534f;
            color: #fff;
        }

        .feature-block h4 {
            font-size: 16px;
            font-weight: 500;
            margin: 3rem 0 1rem;
        }

        .color-info {
            color: #46b8da;
        }

        .color-warning {
            color: #f0ad4e;
        }

        .color-success {
            color: #4cae4c;
        }

        .color-danger {
            color: #d43f3a;
        }

        .btn-custom {
            border: medium none;
            border-radius: 2px;
            cursor: pointer;
            font-size: 14px;
            font-weight: 500;
            letter-spacing: 0;
            margin: 10px 1px;
            outline: 0 none;
            padding: 8px 30px;
            position: relative;
            text-decoration: none;
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <div class="container text-center" style="margin-bottom: 50px;">
        <div class="row">
            <div class="service-heading-block">
                <h2 class="text-center text-divider-double">Service features for our Members</h2>
                <p class="text-center sub-title">
                    become a member, by clicking <a href="Account.aspx"><span class="text-primary">here</span></a><br />
                    and you will enjoy all of our member features mentioned below.
                </p>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="text-center feature-block">
                    <span class="fb-icon color-info">
                        <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    </span>
                    <h4 class="color-info">Cloud Upload</h4>
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dicta error.</p>
                    <a href="javascript:void(0)" class="btn btn-info btn-custom">Click here</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="text-center feature-block">
                    <span class="fb-icon color-warning">
                        <i class="fa fa-desktop" aria-hidden="true"></i>
                    </span>
                    <h4 class="color-warning">Computer Service</h4>
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dicta error.</p>
                    <a href="javascript:void(0)" class="btn btn-warning btn-custom">Click here</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="text-center feature-block">
                    <span class="fb-icon color-success">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                    <h4 class="color-success">Email Service</h4>
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dicta error.</p>
                    <a href="javascript:void(0)" class="btn btn-success btn-custom">Click here</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="text-center feature-block">
                    <span class="fb-icon color-danger">
                        <i class="fa fa-fire" aria-hidden="true"></i>
                    </span>
                    <h4 class="color-danger">Quality Service</h4>
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dicta error.</p>
                    <a href="javascript:void(0)" class="btn btn-danger btn-custom">Click here</a>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
