﻿<%@ Page Language="C#" AutoEventWireup="true" enableEventValidation="false" CodeBehind="passReset.aspx.cs" Inherits="MACEG.passReset" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reset Password | M.A.C. Egypt</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap_css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div id="passwordreset" style="margin-top: 50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">Create New Password</div>
                    </div>
                    <div class="panel-body">
                        <form id="signupform" class="form-horizontal" role="form">
                            <div class="form-group col-sm-12">
                                <label for="email" class=" control-label col-sm-3">Registered email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="txtMail" runat="server" name="email" placeholder="Please input your email used to register with us">
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="email" class=" control-label col-sm-3">New password</label>
                                <div class="col-sm-9">
                                    <input type="password" runat="server" id="txtPass" class="form-control" name="password" placeholder="create your new password">
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="email" class=" control-label col-sm-3">Confirm password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="confirm your new password">
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <!-- Button -->
                                <div class="  col-sm-offset-3 col-sm-9">
                                    <button  type="button" runat="server" id="btnReset" onserverclick="btnReset_ServerClick" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
