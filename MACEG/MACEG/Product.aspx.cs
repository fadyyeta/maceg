﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MACEG
{
    public partial class Product : System.Web.UI.Page
    {
        ClassCode codeClass = new ClassCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadProduct();
            }
        }
        protected void loadProduct()
        {
            if (Request.QueryString["ID"] == null || Request.QueryString["ID"].Trim() == "")
            {
                Response.Redirect("Shop.aspx");
            }
            else
            {
                DataSet ds = codeClass.SQLREAD("select *,dbo.getTagsy(Product.ID) as tagsy ,(select name from Category where Product.categoryID = Category.ID) as catID,(select name from Brand where Product.BrandID = Brand.ID) as BrandName, (select name from LOB where Product.LOBID = LOB.ID) as LOBName from Product where ID = " + Request.QueryString["ID"].ToString());
                lvProdData.DataSource = ds;
                lvProdData.DataBind();
                productLbl.Text = ds.Tables[0].Rows[0]["name"].ToString();
            }

        }

        protected void lvProdData_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {


                ListView lvSpecs = e.Item.FindControl("lvSpecs") as ListView;
                DataSet ds = codeClass.SQLREAD("select * from specs where productID = "+ Request.QueryString["ID"].ToString());
                lvSpecs.DataSource = ds;
                lvSpecs.DataBind();
                //ddl.SelectedIndexChanged += new EventHandler(ddlFieldType_SelectedIndexChanged);

            }
        }
    }
}